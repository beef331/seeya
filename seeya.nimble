# Package

version       = "0.1.0"
author        = "Jason Beetham"
description   = "A new awesome nimble package"
license       = "MIT"
srcDir        = "src"
bin           = @["seeya"]


# Dependencies

requires "nim >= 1.6.10"
