
import compiler / [
  idents, options, modulegraphs, passes,
  lineinfos, sem, pathutils, ast, parser, astalgo,
  cmdlinehelper, commands, main, condsyms, extccomp,
  modules, passaux, platform, nimeval, renderer, ccgutils, cgendata, types
  ]

import std/[os, osproc, times, strformat, strutils, streams, enumerate, sets]

type
  SeeYaGen = object of PPassContext
    module: PSym
    graph: ModuleGraph
    config: ConfigRef
    generatedCode: Stream # Replace with file stream ?
    generatedTypes: HashSet[string]

  BModule = ref SeeYaGen

proc printIndentedComment(strm: Stream, str: string) =
  if str.len > 0:
    let strmStart = strm.getPosition()
    var lineCount = -1;
    while (let pos = strm.getPosition(); pos) > 0 and strm.readChar() notin Newlines:
      strm.setPosition(pos - 1)
      inc lineCount

    strm.setPosition(strmStart);

    let spacedLine =
      if lineCount > 0:
        " ".repeat(lineCount)
      else:
        ""
    for i, line in enumerate splitLines(str):
      if line.len > 0:
        if i > 0:
          strm.write spacedLine
        strm.write "//", line, "\n"

proc semanticPasses(g: ModuleGraph) =
  registerPass g, verbosePass
  registerPass g, semPass


proc newModule(g: ModuleGraph; module: PSym): BModule =
  ## Create a new JS backend module node.
  new(result)
  result.module = module
  result.graph = g
  result.config = g.config

const
  irrelevantForBackend = {tyGenericBody, tyGenericInst, tyGenericInvocation,
                          tyDistinct, tyRange, tyStatic, tyAlias, tySink,
                          tyInferred, tyOwned}


proc mapSetType(conf: ConfigRef; typ: PType): TCTypeKind =
  case int(getSize(conf, typ))
  of 1: result = ctInt8
  of 2: result = ctInt16
  of 4: result = ctInt32
  of 8: result = ctInt64
  else: result = ctArray


proc mapType(conf: ConfigRef; typ: PType; kind: TSymKind): TCTypeKind =
  ## Maps a Nim type to a C type
  case typ.kind
  of tyNone, tyTyped: result = ctVoid
  of tyBool: result = ctBool
  of tyChar: result = ctChar
  of tyNil: result = ctPtr
  of tySet: result = mapSetType(conf, typ)
  of tyOpenArray, tyVarargs:
    if kind == skParam: result = ctArray
    else: result = ctStruct
  of tyArray, tyUncheckedArray: result = ctArray
  of tyObject, tyTuple: result = ctStruct
  of tyUserTypeClasses:
    doAssert typ.isResolvedUserTypeClass
    return mapType(conf, typ.lastSon, kind)
  of tyGenericBody, tyGenericInst, tyGenericParam, tyDistinct, tyOrdinal,
     tyTypeDesc, tyAlias, tySink, tyInferred, tyOwned:
    result = mapType(conf, lastSon(typ), kind)
  of tyEnum:
    if firstOrd(conf, typ) < 0:
      result = ctInt32
    else:
      case int(getSize(conf, typ))
      of 1: result = ctUInt8
      of 2: result = ctUInt16
      of 4: result = ctInt32
      of 8: result = ctInt64
      else: result = ctInt32
  of tyRange: result = mapType(conf, typ[0], kind)
  of tyPtr, tyVar, tyLent, tyRef:
    var base = skipTypes(typ.lastSon, typedescInst)
    case base.kind
    of tyOpenArray, tyArray, tyVarargs, tyUncheckedArray: result = ctPtrToArray
    of tySet:
      if mapSetType(conf, base) == ctArray: result = ctPtrToArray
      else: result = ctPtr
    else: result = ctPtr
  of tyPointer: result = ctPtr
  of tySequence: result = ctNimSeq
  of tyProc: result = if typ.callConv != ccClosure: ctProc else: ctStruct
  of tyString: result = ctNimStr
  of tyCstring: result = ctCString
  of tyInt..tyUInt64:
    result = TCTypeKind(ord(typ.kind) - ord(tyInt) + ord(ctInt))
  of tyStatic:
    if typ.n != nil: result = mapType(conf, lastSon typ, kind)
    else: doAssert(false, "mapType: " & $typ.kind)
  else: doAssert(false, "mapType: " & $typ.kind)

proc typeName(typ: PType): string =
  let typ = typ.skipTypes(irrelevantForBackend)
  typ.sym.name.s

proc cOpen(graph: ModuleGraph; s: PSym; idgen: IdGenerator): PPassContext =
  result = newModule(graph, s)
  result.idgen = idgen

proc generate*(n: PNode, generatedTypes: var HashSet[string], result: Stream, config: ConfigRef)
proc generatedTypeNameKind(typ: PType, config: ConfigRef, fieldName = ""): (string, TCTypeKind)

proc generateProcName(typ: PType, config: ConfigRef): string =
  result = "proc_"
  result.add:
    if typ.n.typ.isNil:
      "void"
    else:
      typ.n.typ.generatedTypeNameKind(config)[0]

  result.add"_"

  if typ.n.len > 3:
    result.add "_"
    for i in 1..<typ.n[paramsPos].len:
      result.add typ.n[paramsPos][i][^2].typ.generatedTypeNameKind(config)[0]
      result.add "_"
  else:
    result.add "void"


proc generateProcField(typ: PType, config: ConfigRef, fieldName: string): string =
  result =
    if typ.n.typ.isNil:
      "void"
    else:
      typ.n.typ.generatedTypeNameKind(config)[0]

  result.add " (*$1)(" % fieldName
  if typ.n.len > 3:
    for i in 1..<typ.n[paramsPos].len:
      result.add typ.n[paramsPos][i][^2].typ.generatedTypeNameKind(config)[0]
      if i < typ.n.len - 1:
        result.add ", "
  else:
    result.add "void"
  result.add ")"


proc generatedTypeNameKind(typ: PType, config: ConfigRef, fieldName = ""): (string, TCTypeKind) =
  result[1] = config.mapType(typ, skVar)
  case result[1]
  of ctInt..ctInt64, ctUint..ctUint64:
    const intTable =[
      ctInt: "intptr_t",
      ctInt8: "int8_t",
      ctInt16: "int16_t",
      ctInt32: "int32_t",
      ctInt64: "int64_t"
      ]
    if result[1] in ctUint .. ctUInt64:
      result[0] = "u"
      result[0].add intTable[TCTypeKind(ord(ctInt) + (ord(result[1]) - ord(ctUint)))]
    else:
      result[0].add intTable[result[1]]
  of ctFloat, ctFloat64:
    result[0] = "double"
  of ctFloat32:
    result[0] = "float"
  of ctNimStr:
    result[0] = "nim_string"
  of ctNimSeq:
    result[0] = "nim_seq_"
    result[0].add generatedTypeNameKind(typ.skipTypes(abstractVar)[0], config)[0]
  of ctPtr:
    result[0].add generatedTypeNameKind(typ[0], config)[0]
    result[0].add " *"
  of ctProc:
    if fieldName.len > 0:
      result[0] = generateProcField(typ, config, fieldName)
    else:
      result[0] = generateProcName(typ, config)

  of ctVoid:
    result[0] = "void"
  of ctStruct:
    case typ.kind
    of tyTuple:
      result[0] = "nim_tuple_"
      for x in typ.sons:
        result[0].add x.generatedTypeNameKind(config)[0]
        result[0].add"_"
      result[0].setLen(result[0].high)
    of tyOpenArray:
      result[0].add typ[0].generatedTypeNameKind(config)[0]
      result[0].add "* openArray_data_$1, intptr_t" % fieldName

    else:
      echo "Defaulting for ", typ.kind, " ", typ
      result[0] = typ.typeName()
  else:
    result[0] = typ.typeName()

proc generateType(typ: PType, generatedTypes: var HashSet[string], result: Stream, config: ConfigRef)

proc generateTypeDef(typ: PType, generatedTypes: var HashSet[string], result: Stream, config: ConfigRef, name: string) =
  let
    isTuple = typ.kind == tyTuple
    name =
      if isTuple:
        for x in typ.sons:
          generateType(x, generatedTypes, result, config)
        generatedTypeNameKind(typ, config)[0]
      else:
        for x in typ.n:
          generateType(x.typ, generatedTypes, result, config)
        name

  result.write "typedef struct $#\n{\n" % name

  if isTuple:
    for i, x in enumerate typ.sons:
      let
        paramName = "field" & $i
        (typName, kind) = generatedTypeNameKind(x, config, paramName)
      if kind != ctProc:
        result.write "    ", typName, " ", paramName
      else:
        result.write "    ", typName
      result.write ";\n"
  else:
    for x in typ.n:
      let
        paramName = x.sym.name.s
        (typName, kind) = generatedTypeNameKind(x.typ, config, paramName)
      if kind != ctProc:
        result.write "    ", typName, " ", paramName
      else:
        result.write "    ", typName
      result.write ";\n"

  result.write "} $#;\n\n" % name


proc generateSeq(typ: PType, generatedTypes: var HashSet[string], result: Stream, config: ConfigRef) =
  const defaultSeq = """typedef struct nim_seq_$1_internal
{
  intptr_t capacity;
  $1 data[];
} nim_seq_$1_internal;

typedef struct nim_seq_$1
{
  intptr_t len;
  nim_seq_$1_internal* data;
} nim_seq_$1;

"""

  let
    baseType = typ[0].skipTypes({tyAlias})
    (name, _) = generatedTypeNameKind(baseType, config)
  if name notin generatedTypes:
    generateType(baseType, generatedTypes, result, config)

  result.write defaultSeq % name

proc generateProc(n: PNode, generatedTypes: var HashSet[string], result: Stream, config: ConfigRef) =
  let
    typ = n[0].sym.typ
    hasType = typ.n.typ != nil
  if hasType:
    generateType(typ.n.typ, generatedTypes, result, config)

  for i in 1 ..< n[paramsPos].len:
    generateType(n[paramsPos][i][^2].typ, generatedTypes, result, config)

  for x in n[bodyPos]:
    if x.kind == nkCommentStmt:
      printIndentedComment(result, x.comment)

  if hasType:
    result.write typ.n.typ.generatedTypeNameKind(config)[0], " "
  else:
    result.write "void "

  result.write n[0].sym.name.s, "("
  var wrote = false

  for i in 1 ..< n[paramsPos].len:
    for j in 0 ..< n[paramsPos][i].len - 2:
      let
        param = n[paramsPos][i][j]
        (name, kind) = generatedTypeNameKind(n[paramsPos][i][^2].typ, config, param.sym.name.s)
      result.write name
      if kind != ctProc:
        result.write " ", param.sym.name.s
      result.write ", "
      wrote = true

  if wrote:
    result.setPosition(result.getPosition() - 2);
  result.write ");\n\n"

proc generateOpenArray(typ: PType, generatedTypes: var HashSet[string], result: Stream, config: ConfigRef) =
  let element = typ[0]
  generateType(element, generatedTypes, result, config)


proc generateType(typ: PType, generatedTypes: var HashSet[string], result: Stream, config: ConfigRef) =
  let
    typ = typ.skipTypes({tyAlias})
    (name, cKind) = generatedTypeNameKind(typ, config)
  if name notin generatedTypes:
    case cKind
    of ctStruct:
      case typ.kind
      of tyTuple, tyObject:
        generateTypeDef(typ, generatedTypes, result, config, name)
      of tyOpenArray:
        generateOpenArray(typ, generatedTypes, result, config)
      else:
        echo "Cannot generate type definition for: ", typ.kind, " which is ", typ

    of ctNimSeq:
      generateSeq(typ, generatedTypes, result, config)
    of ctPtr:
      discard
    of ctArray:
      discard
    else:
      discard
    generatedTypes.incl name

proc generateType(n: PNode, generatedTypes: var HashSet[string], result: Stream, config: ConfigRef) =
  generateType(n, generatedTypes, result, config)

proc generateTypeDef*(n: PNode, generatedTypes: var HashSet[string], result: Stream, config: ConfigRef) =
  var buffer = newStringStream(newStringOfCap(128)) # We use a buffer so we can write new types then add this struct, sort of a delayed addition
  case n[^1].kind
  of nkObjectTy:
    n[0][0].typ.generateType(generatedTypes, result, config)
  of nkEnumTy: discard
  else: echo "generateTypeDef, cannot generate: ", n.kind

const defaultGeneratedFile ="""
#include "stdint.h"

typedef struct nim_string_internal
{
    intptr_t cap;
    char data[];
} nim_string_internal;

typedef struct nim_string
{
    intptr_t len;
    nim_string_internal* data;
} nim_string;

"""

proc generate*(n: PNode, generatedTypes: var HashSet[string], result: Stream, config: ConfigRef) =
  case n.kind
  of nkProcDef, nkFuncDef:
    if {sfExportc, sfCompilerProc} * n[0].sym.flags == {sfExportc}:
      generateProc(n, generatedTypes, result, config)
  of nkTypeSection, nkStmtList:
    for typ in n:
      generate(typ, generatedTypes, result, config)
  of nkTypeDef:
    if n[0].kind == nkPragmaExpr and sfExportC in n[0][0].sym.flags:
      generateTypeDef(n, generatedTypes, result, config)

  of nkObjectTy: discard
  of nkRefTy: discard
  of nkEnumTy: discard
  else:
    discard

proc cProcess(b: PPassContext, n: PNode): PNode =
  let module = BModule(b)
  if module.generatedCode.isNil:
    module.generatedCode = newFileStream("tests/testlib.h", fmReadWrite)
    module.generatedCode.write defaultGeneratedFile

  if sfSystemModule notin module.module.flags:
    generate(n, module.generatedTypes, module.generatedCode, module.config)
  result = n

proc cClose(graph: ModuleGraph; b: PPassContext, n: PNode): PNode = discard


const
  CGenPass = makePass(cOpen, cProcess, cClose)

proc commandCompileToC(graph: ModuleGraph) =
  let conf = graph.config
  extccomp.initVars(conf)
  semanticPasses(graph)

  graph.registerPass CGenPass


  if not extccomp.ccHasSaneOverflow(conf):
    conf.symbols.defineSymbol("nimEmulateOverflowChecks")

  compileProject(graph)



proc processCmdLine(pass: TCmdLinePass, cmd: string; config: ConfigRef) =
  echo cmd



proc getNimLibPath: string =
  let (nimOutput, nimExitCode) = execCmdEx(
    "nim --verbosity:0 --eval:\"import std/os; echo getCurrentCompilerExe()\"",
    options={poUsePath}
  )

  if nimExitCode != 0:
    echo nimOutput
    raise newException(IoError, fmt"Error while trying to locate nim executable (exit code {nimExitCode})")

  result = nimOutput.parentDir.parentDir / "lib"


proc mainCommand*(graph: ModuleGraph) =
  let conf = graph.config
  let cache = graph.cache

  # In "nim serve" scenario, each command must reset the registered passes
  clearPasses(graph)
  conf.lastCmdTime = epochTime()


  proc customizeForBackend(backend: TBackend) =
    ## Sets backend specific options but don't compile to backend yet in
    ## case command doesn't require it. This must be called by all commands.
    defineSymbol(graph.config.symbols, $conf.backend)
    conf.backend = backendC


  customizeForBackend(conf.backend)
  conf.outFile = RelativeFile string(conf.projectFull).changeFileExt("h")
  echo string(conf.outFile)
  commandCompileToC(graph)



proc seeya*(fileName: string) =
  let
    conf = newConfigRef()
    self = NimProg(
      supportsStdinFile: true,
      processCmdLine: processCmdLine
    )
    cache = newIdentCache()

  self.initDefinesProg(conf, "nim_compiler")
  self.initDefinesProg(conf, "seeya")
  conf.libPath = AbsoluteDir getNimLibPath()
  conf.searchPaths.add(conf.libpath)
  conf.projectFull = AbsoluteFile paramStr(1)
  var graph = newModuleGraph(cache, conf)

  if conf.selectedGC == gcUnselected:
    if conf.backend in {backendC, backendCpp, backendObjc}:
      initOrcDefines(conf)

  mainCommand(graph)

  if conf.errorCounter != 0: return

seeya("test.nim")
