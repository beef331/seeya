
{.pragma: seeya, exportc, dynlib, cdecl.}
type
  MyType {.exportc.} = object ## This is an object
    x, y: int ##[
      These do things
      that are very
      important
    ]##
    z: float
    w: string ## This does aswell
    u: seq[int]
    v: proc(): int {.cdecl.}
    a: (int, int, (string, string, float))
    b: (proc(): int{.cdecl.},)
  MyEnum {.exportc.} = enum a, b, c

proc doThing(i: int, myType: MyType)  {.seeya.} =
  ## This does some magic stuff
  ## You may want to consider that
  echo i, " ", myType

proc myThing() {.seeya.} =
  ##[
    Hmmmmmm Things and stuff
  ]##

  discard


proc myProc(s: var seq[int], y: seq[string]): int {.seeya.} = discard

proc makeType(x, y: int, z: float): MyType {.seeya.} = MyType(x: x, y: y, z: z)

proc someLent(a: string): lent string {.seeya.} = a

proc someArray(oa: openarray[int]) {.seeya.} = echo oa
