#include "stdint.h"

typedef struct nim_string_internal
{
    intptr_t cap;
    char data[];
} nim_string_internal;

typedef struct nim_string
{
    intptr_t len;
    nim_string_internal* data;
} nim_string;

typedef struct nim_seq_intptr_t_internal
{
  intptr_t capacity;
  intptr_t data[];
} nim_seq_intptr_t_internal;

typedef struct nim_seq_intptr_t
{
  intptr_t len;
  nim_seq_intptr_t_internal* data;
} nim_seq_intptr_t;

typedef struct nim_tuple_nim_string_nim_string_double
{
    nim_string field0;
    nim_string field1;
    double field2;
} nim_tuple_nim_string_nim_string_double;

typedef struct nim_tuple_intptr_t_intptr_t_nim_tuple_nim_string_nim_string_double
{
    intptr_t field0;
    intptr_t field1;
    nim_tuple_nim_string_nim_string_double field2;
} nim_tuple_intptr_t_intptr_t_nim_tuple_nim_string_nim_string_double;

typedef struct nim_tuple_proc_intptr_t_void
{
    intptr_t (*field0)(void);
} nim_tuple_proc_intptr_t_void;

typedef struct MyType
{
    intptr_t x;
    intptr_t y;
    double z;
    nim_string w;
    nim_seq_intptr_t u;
    intptr_t (*v)(void);
    nim_tuple_intptr_t_intptr_t_nim_tuple_nim_string_nim_string_double a;
    nim_tuple_proc_intptr_t_void b;
} MyType;

//This does some magic stuff
//You may want to consider that
void doThing(intptr_t i, MyType myType);

//Hmmmmmm Things and stuff
void myThing();

typedef struct nim_seq_nim_string_internal
{
  intptr_t capacity;
  nim_string data[];
} nim_seq_nim_string_internal;

typedef struct nim_seq_nim_string
{
  intptr_t len;
  nim_seq_nim_string_internal* data;
} nim_seq_nim_string;

intptr_t myProc(nim_seq_intptr_t * s, nim_seq_nim_string y);

MyType makeType(intptr_t x, intptr_t y, double z);

nim_string * someLent(nim_string a);

void someArray(intptr_t* openArray_data_oa, intptr_t oa);

