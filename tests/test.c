#include "testlib.h"
#include <stdlib.h>

int main(){
  intptr_t *data = (intptr_t*)(malloc(sizeof(intptr_t) * 100));
  for(int i = 0; i < 100; i++){
    data[i] = i;
  }
  someArray(data, 100);
  return 0;
}
